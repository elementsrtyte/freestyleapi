﻿CREATE TABLE [dbo].[GamesWords]
(
	[GameId] INT NOT NULL,
	[WordId] INT NOT NULL, 
	FOREIGN KEY(WordId) REFERENCES Words(Id),
	FOREIGN KEY(GameId) REFERENCES Games(Id),
	
)
GO
CREATE CLUSTERED INDEX gwIndex ON GamesWords(GameId)