﻿CREATE TABLE [dbo].[Users]
(
	[Id] INT IDENTITY(1,1) NOT NULL PRIMARY KEY, 
    [Username] VARCHAR(20) NOT NULL, 
    [Password] VARCHAR(30) NOT NULL,
	[Email] VARCHAR(50) NULL, 
    [DateAdded] DATETIME  DEFAULT (GETDATE()) NOT NULL, 
    [DateUpdated] DATETIME NULL, 
    Unique(Username, Email)
)
