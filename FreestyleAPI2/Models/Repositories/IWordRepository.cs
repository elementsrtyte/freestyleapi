﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreestyleAPI2.Models
{
    interface IWordRepository
    {
        IEnumerable<Word> GetAll();
        List<Word> GetSelectedAmount(int amount);
        //Word Add(Word item);
    }
}
