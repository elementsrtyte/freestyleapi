﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreestyleAPI2.Models
{
    interface IUserRepository
    {
        IEnumerable<User> GetAll();
        User Add(User item);
        User AuthenticateUser(String username, String password);
    }
}
