﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreestyleAPI2.Models
{
    interface IGameRepository
    {
        IEnumerable<Game> GetGames(String username);
        Game CreateGame(Game game);
        bool UpdateGame(Game game);
        void InsertRecording(Recording recording);
        IEnumerable<Recording> GetRecordings(int gid);
        void UpdateStatus(Game game);
        Boolean CheckGameEnd(Game game);
    }
}
