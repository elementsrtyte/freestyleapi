﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FreestyleAPI2.Models
{
    public class UserRepository : IUserRepository
    {
        List<User> users = new List<User>();
        int _nextId = 0;

        public UserRepository()
        {
            Add(new User { Username = "Neil", Password = "password1" });
            Add(new User { Username = "Adam", Password = "password1" });
            Add(new User { Username = "Bob", Password = "password1" });
        }

        public IEnumerable<User> GetAll()
        {
            return users;
        }

        public User Add(User item)
        {
            if (item == null)
                throw new ArgumentNullException("item");
            users.Add(item);
            item.Id = _nextId++;
            return item;
        }

        public User AuthenticateUser(String username, String password)
        {
            User user = users.Find(u => (u.Username == username && u.Password == password));
            if (user == null) 
                return null;
            return user; 
        }
    }
}