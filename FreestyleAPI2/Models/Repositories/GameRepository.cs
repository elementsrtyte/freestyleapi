﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;
using Dapper;
using DapperExtensions;
using System.Data.Sql;

namespace FreestyleAPI2.Models.Repositories
{



    public class GameRepository : IGameRepository
    {
        String connStr = ConfigurationManager.ConnectionStrings["azureConn"].ConnectionString;

        public IEnumerable<Game> GetGames(String username)
        {
            using (var connection = new SqlConnection(connStr))
            {
                connection.Open();

                string sql = "SELECT g.*, w.* FROM Games g inner join GamesWords gw on g.id = gw.GameId inner join Words w on gw.WordId = w.Id where Player1 = @Username or Player2 = @username";
                
                Dictionary<int, Game> lookup = new Dictionary<int, Game>();
                IEnumerable<Game> item = connection.Query<Game, Word, Game>(sql, (g, w) =>
                {
                    Game game;
                    if (!lookup.TryGetValue(g.Id, out game))
                    {
                        lookup.Add(g.Id, game = g);
                    }
                    if (game.Words == null)
                        game.Words = new List<Word>();
                    game.Words.Add(w);
                    return game;
                }, new { username }).AsEnumerable();

                return lookup.Values.AsEnumerable<Game>();
            }
        }

        public Game CreateGame(Game game)
        {
            if (game == null)
                throw new ArgumentNullException("game");

            using(var connection = new SqlConnection(connStr))
            {
                connection.Open();

                //Insert into GamesDB
                string gamesQuery = @"INSERT INTO GAMES ([Player1],[Player2],[CurrentUser],[Status]) VALUES (@Player1,@Player2,@CurrentUser,@Status)
                                      SELECT CAST(SCOPE_IDENTITY() as INT)";
                var id = connection.Query<int>(gamesQuery,
                    new
                    {
                        game.Player1,
                        game.Player2,
                        game.CurrentUser,
                        game.Status
                    }).Single();

                game.Id = id;
    
                //Insert into relationship many-many DB
                string gamewordsQuery = "INSERT INTO GamesWords (gameId, wordId) VALUES(@gameId, @wordId)";
                foreach (var w in game.Words.ToList<Word>())
                {
                    connection.Execute(gamewordsQuery,
                        new { gameId = id, wordId = w.Id }
                        );
                }
            }

            
            return game;
 
            //game.Id = _nextItem++;
            //game.Words = new List<String> { "new", "Swag", "beer" };

            //repository.Add(game);
            //return game;
        }

        public bool UpdateGame(Game game)
        {
            if (game == null)
                return false;
       //     repository.Find(g => g.Id == game.Id).Status = game.Status;
            return true;

        }

        public void InsertRecording(Recording recording)
        {
            using (var connection = new SqlConnection(connStr))
            {
                connection.Open();

                //TODO: Refactor this so that there is validation that there is a max of only two recordings per game.  Use a sql constraint ideally, so i don't have to make an additional SQL call.

                string gamesQuery = @"INSERT INTO RECORDINGS (gameId, username, filename) VALUES(@gameId, @username, @filename)";
                //TODO: Refactor this to later just insert recording without having to manually cast each variable in.
               
                var id = connection.Execute(gamesQuery, new
                {
                    recording.GameId,
                    recording.Username,
                    recording.Filename
                });
                
            }
        }

        public IEnumerable<Recording> GetRecordings(int gameid)
        {
            using (var connection = new SqlConnection(connStr))
            {
                connection.Open();

                string Query = @"SELECT * FROM RECORDINGS WHERE GameId = @GameId";
                return connection.Query<Recording>(Query, new { gameid });

            }
        }

        public void UpdateStatus(Game game)
        {
            using (var connection = new SqlConnection(connStr))
            {
                connection.Open();
                //TODO:  At some point ensure that the logged in user can only affect games that he is currently in.
                string Query = @"UPDATE GAMES SET Status = @status WHERE id = @id";
                var id = connection.Execute(Query, 
                    new {
                        game.Status,
                        game.Id 
                    });

            }
        }

        public Boolean CheckGameEnd(Game game)
        {
            using (var connection = new SqlConnection(connStr))
            {
                //TODO:  Come up with a better way to check for GameEnd.. this could fall apart if a user ever gets a chance to accidentally submit two recordings..
                connection.Open();
                string gamesQuery = @"SELECT COUNT(*) FROM Recordings WHERE GameID = @id";

                var count = connection.Query<int>(gamesQuery, new { game.Id }).Single();
                if (count >= 2)
                    return true;
                else
                    return false;
            }
        }
    }
}