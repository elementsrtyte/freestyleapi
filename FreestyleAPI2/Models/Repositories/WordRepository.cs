﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;
using DapperExtensions;
using System.Data.Sql;

namespace FreestyleAPI2.Models
{
    public class WordRepository : IWordRepository
    {
        String connStr = ConfigurationManager.ConnectionStrings["azureConn"].ConnectionString;
        private List<Word> words = new List<Word>();


        public IEnumerable<Word> GetAll()
        {
            return words;
        }

        public List<Word> GetSelectedAmount(int amount)
        {
            using(var connection = new SqlConnection(connStr))
            {
                //var wordList = connection.GetList<Word>("SELECT TOP 5 * FROM Words order by newid())");
                var wordList = connection.Query<Word>("SELECT TOP 5 * FROM Words order by newid()");
                return wordList.ToList<Word>();
            }
        }
    }
}