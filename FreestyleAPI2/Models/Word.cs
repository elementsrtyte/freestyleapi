﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FreestyleAPI2.Models
{
    public class Word
    {
        public int Id { get; set; }
        public String word { get; set; }
        public DateTime DateAdded { get; set; }
    }
}