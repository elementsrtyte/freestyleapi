﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FreestyleAPI2.Models
{
    public class Recording
    {
        public int Id { get; set; }
        public int GameId { get; set; }
        public String Username { get; set; }
        public String Filename { get; set; }
    }
}