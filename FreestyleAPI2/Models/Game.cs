﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FreestyleAPI2.Models
{
    public class Game
    {
        public int Id { get; set; }
        public String Player1 { get; set; }
        public String Player2 { get; set; }
        public String CurrentUser { get; set; }
        public String Status { get; set; }
        public List<Word> Words { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }


    }
}