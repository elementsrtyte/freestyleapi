﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using FreestyleAPI2.Models;
using System.Security.Principal;

namespace FreestyleAPI2.Security
{
    public class BasicAuthenticationAttribute : System.Web.Http.Filters.ActionFilterAttribute
    {
        static readonly UserRepository repository = new UserRepository();

        // Summary:
        //     Occurs before the action method is invoked.
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            //If the user doesn't attempt to pass any authorization headers in.
            if (actionContext.Request.Headers.Authorization == null)
            {
                actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
            }
            else
            {
	            string authToken = actionContext.Request.Headers.Authorization.Parameter;
	            

                try
                {
                    string decodedToken = Encoding.UTF8.GetString(Convert.FromBase64String(authToken));
                    string username = decodedToken.Substring(0, decodedToken.IndexOf(":"));
                    string password = decodedToken.Substring(decodedToken.IndexOf(":") + 1);

                    User user = repository.AuthenticateUser(username, password);
                    //If the user inputs valid credentials log them in.
                    if (user != null)
                    {
                        HttpContext.Current.User = new GenericPrincipal(new ApiIdentity(user), new string[] { });
                        base.OnActionExecuting(actionContext);

                    }
                    //If the user inputs invalid credentials
                    else
                    {
                        actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
                    }
                }
                catch (Exception)
                {
                    actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
                }

            }
        }
    }
}