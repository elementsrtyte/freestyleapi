﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using FreestyleAPI2.Models;
using FreestyleAPI2.Models.Repositories;
using FreestyleAPI2.Security;


namespace FreestyleAPI2.Controllers
{
    public class GameController : ApiController
    {
        static readonly IGameRepository repository = new GameRepository();
        static readonly IWordRepository wordRepository = new WordRepository();
        // GET api/<controller>
        [BasicAuthentication]
        [HttpGet]
        public IEnumerable<Game> GetGames()
        {
            return repository.GetGames(System.Web.HttpContext.Current.User.Identity.Name);
        }
        
        [BasicAuthentication]
        [HttpPost]
        public Game CreateGame(Game game)
        {
            if(game == null || game.Player2 == null)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            game.Status = "Pending";
            game.Player1 = HttpContext.Current.User.Identity.Name;
            game.CurrentUser = HttpContext.Current.User.Identity.Name;
            game.Words = wordRepository.GetSelectedAmount(5);
            game.EndDate = DateTime.Today.AddDays(7);

            return repository.CreateGame(game);
        }

        [BasicAuthentication]
        [HttpPost]
        public HttpResponseMessage InsertRecording(Recording recording)
        {
            if(recording == null)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            recording.Username = HttpContext.Current.User.Identity.Name;

            repository.InsertRecording(recording);

            return new HttpResponseMessage(HttpStatusCode.Created);
        }

        [BasicAuthentication]
        [HttpPost]
        public IEnumerable<Recording> GetRecordings(Recording rec)
        {
            return repository.GetRecordings(rec.GameId);
        }

        [BasicAuthentication]
        [HttpPut]
        public HttpResponseMessage UpdateStatus(Game game)
        {
            if(game == null)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            repository.UpdateStatus(game);

            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        [BasicAuthentication]
        [HttpPost]
        public Game CheckGameEnd(Game game)
        {
            if (game == null)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
            if (repository.CheckGameEnd(game))
            {
                game.Status = "Finished";
                repository.UpdateStatus(game);
            }
            else
            {
                game.Status = "Pending";
            }
            return game;
            }
    }
}