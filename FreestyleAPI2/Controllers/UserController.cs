﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FreestyleAPI2.Models;

namespace FreestyleAPI2.Controllers
{
    public class UserController : ApiController
    {
        static readonly IUserRepository repository = new UserRepository();

        // POST api/<controller>
        public HttpResponseMessage PostRegister(User user)
        {
            repository.Add(user);
            var response = Request.CreateResponse<User>(HttpStatusCode.Created, user);

            string uri = Url.Link("DefaultApi", new { id = user.Id });
            response.Headers.Location = new Uri(uri);
            return response;
        }
    }
}